﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuestBook.Services;

namespace GuestBook.Initialize
{
    class Program
    {

        static void Main(string[] args)
        {
            // Создаем БД
            CreateDatabase();
            
            const string ConnectionString = "Server=.\\sqlexpress;Database=GuestBook;Trusted_Connection=True;";
            // Отдельным подключением создаем таблицу
            using (var service = new DatabaseService(ConnectionString))
            {
                service.CreateTable();
            }
        }

        private static void CreateDatabase()
        {            
            const string str = "CREATE DATABASE GuestBook";
            using (var myConn = new SqlConnection("Server=.\\sqlexpress;Database=master;Trusted_Connection=True;"))
            {
                var myCommand = new SqlCommand(str, myConn);
                myConn.Open();
                myCommand.ExecuteNonQuery();
                myConn.Close();    
            }
        }
    }
}
