﻿using System.Data;
using System.Data.SqlClient;
using GuestBook.Services;
using Xunit;

namespace GuestBook.Tests
{
    public class DBConnectionTest
    {
        private const string ConnectionString = "Server=.\\sqlexpress;Database=GuestBook;Trusted_Connection=True;";

        [Fact]
        public void ShouldCreateTable()
        {
            using (var service = new DatabaseService(ConnectionString))
            {
                service.CreateTable();
            }
        }
    }
}