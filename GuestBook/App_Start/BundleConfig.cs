﻿using System.Web.Optimization;

namespace GuestBook
{
    public static class BundleConfig
    {
        public static void Configure(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/LibCss")
                .Include("~/Content/bootstrap.min.css")
                .Include("~/Content/bootstrap-theme.min.css")
                );

            bundles.Add(new ScriptBundle("~/LibJs")
                .Include("~/Scripts/jquery-1.9.1.min.js")
                .Include("~/Scripts/jquery.validate.min.js")
                .Include("~/Scripts/jquery.twbsPagination.js")
                .Include("~/Scripts/bootstrap.min.js")
                );

            bundles.Add(new ScriptBundle("~/AppJs")
                .Include("~/Scripts/app.js"));
        }
    }
}