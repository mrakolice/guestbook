﻿using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using FluentValidation.Results;
using GuestBook.DatabaseModels;
using GuestBook.Models;
using GuestBook.Services;

namespace GuestBook.Controllers
{
    public class MessageController:Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetList(int page = 1, bool sortAsc=true, string sortPropertyName=null)
        {
            var messages = DatabaseService.Current
                .GetMessages(page, sortOrder: sortAsc?SortOrder.Ascending : SortOrder.Descending, sortPropertyName: sortPropertyName);

            var models = messages.Select(m => new MessageModel()
                                              {
                                                  Email = m.Email,
                                                  HomePage = m.HomePage,
                                                  Text = m.Text,
                                                  UserName = m.UserName,
                                                  Created = m.Created
                                              }).ToList();
            var result = new
                         {
                             pageCount= DatabaseService.Current.GetPageCount(),
                             list = models
                         };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Insert(MessageModel model)
        {
            var validator = new MessageModelValidator();

            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
            {
                return Json(new {success = false});
            }
            
            var message = new Message(model)
                          {
                              IP = Request.ServerVariables["REMOTE_ADDR"],
                              Browser = Request.Browser.Browser
                          };

            DatabaseService.Current.InsertMessage(message);

            return Json(new{success=true});
        }
    }
}