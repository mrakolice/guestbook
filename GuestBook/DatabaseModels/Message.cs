﻿using System;
using GuestBook.Models;

namespace GuestBook.DatabaseModels
{
    public class Message
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string HomePage { get; set; }

        public string Text { get; set; }

        public string IP { get; set; }

        public string Browser { get; set; }

        public DateTime Created { get; set; }

        public Message()
        {
        }

        public Message(MessageModel messageModel)
        {
            // Вместо этой канители можно использовать Automapper
            // Почему я его не прикрутил? 
            // Потому что бессмысленно для маленького проекта городить подобные инфраструктуры
            UserName = messageModel.UserName;
            Email = messageModel.Email;
            HomePage = messageModel.HomePage;
            Text = messageModel.Text;
            Created = DateTime.Now;
        }
    }
}