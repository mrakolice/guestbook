﻿using System;

namespace GuestBook.Models
{
    public class MessageModel
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string HomePage { get; set; }

        public string Text { get; set; } 

        public DateTime Created { get; set; } 
    }
}