﻿// Волшебно-магический класс, в котором происходят всякие действия с логикой

// А здесь находятся биндинги на реальные кнопочки и тд

function parseMsDate(msDate) {
    if (!msDate) {
        return null;
    }
    var ticks = parseInt(msDate.replace(/\/Date\((\d+)\)\//gi, "$1"));
    if (!ticks) {
        return null;
    }
    if (ticks <= 0) {
        return new Date(Date.now());
    } else {
        return new Date(ticks);
    }
}

var MessageController = function (
    showFormButtonSelector,
    clearFormButtonSelector,
    addButtonSelector,
    cancelButtonSelector,
    messageFormSelector,
    messagesTableSelector,
    paginationSelector,
    alertMessageSelector
    ) {
    var self = this;

    self._alertMessage = $(alertMessageSelector);
    self._showFormButton = $(showFormButtonSelector);
    self._clearFormButton = $(clearFormButtonSelector);
    self._addButton = $(addButtonSelector);
    self._cancelButton = $(cancelButtonSelector);

    self._messageForm = $(messageFormSelector);
    self._messagesTable = $(messagesTableSelector);

    self._pagination = $(paginationSelector);

    self._applyBindings();

    self._currentPage = self.getCurrentPageFromUrl();
    self._isOrderAscending = true;

    $(self._messagesTable.find(".ordered-field")[0]).trigger("click");
};

MessageController.prototype.getCurrentPageFromUrl = function () {
    // Убираем "/"
    var pathName = window.location.pathname.substr(1);
    var parts = pathName.split("/");
    if (parts.length > 1) {
        window.location = "/";
        return 1;
    }

    // волшебная регулярка, найденная на просторах интернета
    if (parts[0].match(/^\d+(\.\d+?)?$/)) {
        return parseInt(parts[0]);
    }
    // Если строчка пустая и не соответствует паттерну
    else if (!parts[0]) {
        return 1;
    }
    //Если строчка непустая и не соответствует паттерну
    else {
        window.location = "/";
        return 1;
    }

   
}

MessageController.prototype._applyBindings = function () {
    var self = this;

    self._showFormButton.click(function() { self.showForm(); });
    self._clearFormButton.click(function () { self.clearForm(); });
    self._addButton.click(function () { self.addMessage(); });
    self._cancelButton.click(function () { self.cancel(); });

    self._messagesTable.find(".ordered-field").click(function () {
        self.getOrderedMessages($(this));
    });

    self._messageForm.parent().hide();

    self._messageForm.validate({
        rules: {
            UserName: {
                required: true,
                accept: "[0-9a-zA-Z]+"
            },
            Email: {
                required: true,
                email:true
            },
            HomePage: {
                url:true
            },
            Text: {
                required: true,
                accept: "[^\<\>\{\}]+"
            }
        },
        messages: {
            UserName: {
                accept: "Only latin letters and numbers are acceptable"
            },
            Text: {
                accept: "Curly brackets are not accepted"
            }
        }
    });
}

MessageController.prototype.getOrderedMessages = function(element) {
    var self = this;
    // Получаем имя поля
    var propertyName = element.text();

    // Получаем текущий порядок
    // Если имя поля совпадает с предыдущим, то меняем порядок
    // Если имя поля не совпадает с предыдущим, то ставим порядок в 0
    if (self._currentOrderProperty !== propertyName) {
        self._isOrderAscending = true;
        self._currentOrderProperty = propertyName;
    } else {
        self._isOrderAscending = !self._isOrderAscending;
    }
    //Добавляем волшебный спан
    self._messagesTable.find('thead th span').remove();

    var span =
        self._isOrderAscending ?
        "<span class='glyphicon glyphicon-chevron-up' aria-hidden='true'></span>":
        "<span class='glyphicon glyphicon-chevron-down' aria-hidden='true'></span>";
    
    element.append(span);

    self._getMessages(self._currentPage, self._isOrderAscending, self._currentOrderProperty);
}

MessageController.prototype.showForm = function () {
    var self = this;

    self._messagesTable.parent().hide();
    self._messageForm.parent().show();
}

MessageController.prototype.clearForm = function () {
    var self = this;
    self._messageForm[0].reset();
}

MessageController.prototype.addMessage = function () {
    var self = this;
    // Парсим в JSON и гоним по ajax
    
    if (!self._messageForm.valid()) {
        return;
    }

    var data = {
        UserName: self._messageForm.find("#UserName").val(),
        Email: self._messageForm.find("#Email").val(),
        HomePage: self._messageForm.find("#HomePage").val(),
        Text: self._messageForm.find("#Text").val()
    };
    $.ajax({
        url: "/Message/Insert",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (response) {
            if (response.success) {
                self.cancel();
                self._getMessages(self._currentPage, self._currentOrder, self._currentOrderProperty);
            } else {
                self._alertMessage.removeClass('hidden');
            }
        }
    });
}

MessageController.prototype.cancel = function () {
    var self = this;
    self.clearForm();
    self._messageForm.parent().hide();
    self._messagesTable.parent().show();
    self._alertMessage.addClass("hidden");
}

MessageController.prototype._getMessages = function(pageNum, order, orderPropertyName) {
    // Здесь идем на сервак и слезно просим дать нам хоть какие-то данные из БД
    // Может быть, их даже дадут
    var self = this;
    var data = {
        page:pageNum,
        sortAsc:order,
        sortPropertyName:orderPropertyName
    };
    $.ajax({
        url: "/Message/GetList",
        type: "GET",
        data: data,
        contentType: "application/json",
        dataType:"json",
        success: function(response) {
            self._messagesTable.find("tbody tr").remove();

            $.each(response.list, function(index, message) {
                var row = "<tr>";
                row += "<td>" + message.UserName + "</td>";
                row += "<td>" + message.Email + "</td>";
                row += "<td>" + parseMsDate(message.Created).toLocaleString() + "</td>";
                row += "<td>" + message.HomePage + "</td>";
                row += "<td>" + message.Text + "</td>";
                row += "</tr>";

                self._messagesTable.find("tbody").append(row);
            });

            if (self._pagination.data("twbs-pagination")) {
                self._pagination.twbsPagination('destroy');
            }

            self._pagination.twbsPagination({
                totalPages: response.pageCount,
                startPage:self._currentPage,
                visiblePages: 7,
                initiateStartPageClick:false,
                onPageClick: function (event, page) {
                    self.gotoPage(page);
                }
            });
        }
    });
}

MessageController.prototype.gotoPage = function (pageNum) {
    var self = this;
    self._currentPage = pageNum;
    if (history) {
        history.pushState({ prevPage: pageNum }, "page " + pageNum, pageNum.toString());
    }
    self._getMessages(pageNum, self._isOrderAscending, self._currentOrderProperty);
}
// showFormButton messagesTable arrowSpan previousButton nextButton

jQuery.validator.addMethod("accept", function (value, element, param) {
    return value.match(new RegExp(param + "$"));
});

$(document).ready(function () {
    var messageCtrl = new MessageController(
        "#showFormButton",
        "#clearButton",
        "#addButton",
        "#cancelButton",
        "#messageForm",
        "#messagesTable",
        "#pagination",
        "#alertMessage"
     );
})