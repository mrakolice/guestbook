﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using GuestBook.DatabaseModels;
using MigSharp;

namespace GuestBook.Services
{
    public class DatabaseService:IDisposable
    {
        private static DatabaseService _current;
        public const int DefaultPageSize = 25;


        public static DatabaseService Current
        {
            get
            {
                if (_current == null)
                {
                    // А вот если не будет строки подключения, то да, будет очень грустно...
                    _current = new DatabaseService(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                }

                return _current;
            }
        }
        
        private readonly SqlConnection _connection;
        private readonly DbSchema _dbSchema;

        private readonly List<string> _availableProperties = new List<string>()
                                                             {
                                                                 "UserName",
                                                                 "Email",
                                                                 "Created"
                                                             }; 

        public DatabaseService(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();

            _dbSchema = new DbSchema(string.Empty, DbPlatform.SqlServer2012);
            _dbSchema.UseCustomConnection(_connection);
        }

        public void CreateTable()
        {
            _dbSchema.Alter(db => db.Tables["Messages"].DropIfExists());

            _dbSchema.Alter(db => db.CreateTable("Messages")
                .WithPrimaryKeyColumn("Id", DbType.Int32).AsIdentity()
                .WithNotNullableColumn("UserName", DbType.String)
                .WithNotNullableColumn("Email", DbType.String)
                .WithNullableColumn("Homepage", DbType.String)
                .WithNotNullableColumn("Created", DbType.DateTime)
                .WithNotNullableColumn("Text", DbType.String)
                .WithNotNullableColumn("IP", DbType.String)
                .WithNotNullableColumn("Browser", DbType.String)
                );
        }

        public void InsertMessage(Message message)
        {
            // Потому что а не пофиг ли? Если нужно, расширим. А так быстрее.
            var sqlString = @"
               INSERT INTO Messages 
		    (
			    [UserName],
			    [Email],
			    [HomePage],
			    [Created],
			    [Text],
			    [IP],
			    [Browser]
		    )
	        VALUES 
		    (
			@UserName,
		    @Email,
			@HomePage,
			@Created,
			@Text,
			@IP,
			@Browser
		    );
            ";

            sqlString = string.Format(sqlString, message.UserName, message.Email, message.HomePage, message.Text, message.IP,
                message.Browser, message.Created);

            var command = new SqlCommand(sqlString, _connection);
            command.Parameters.Add("@UserName", SqlDbType.NVarChar);
            command.Parameters.Add("@Email", SqlDbType.NVarChar);
            command.Parameters.Add("@HomePage", SqlDbType.NVarChar);
            command.Parameters.Add("@Created", SqlDbType.DateTime);
            command.Parameters.Add("@Text", SqlDbType.NVarChar);
            command.Parameters.Add("@IP", SqlDbType.NVarChar);
            command.Parameters.Add("@Browser", SqlDbType.NVarChar);

            command.Parameters["@UserName"].Value = message.UserName;
            command.Parameters["@Email"].Value = message.Email;
            command.Parameters["@HomePage"].Value = message.HomePage ?? string.Empty;

            command.Parameters["@Created"].Value = message.Created;
            command.Parameters["@Text"].Value = message.Text;
            command.Parameters["@IP"].Value = message.IP;
            command.Parameters["@Browser"].Value = message.Browser;

            command.ExecuteNonQuery();
        }

        public List<Message> GetMessages(int page=1, int pageSize=DefaultPageSize, SortOrder sortOrder = SortOrder.Unspecified, string sortPropertyName=null)
        {
            var sqlString = @"
                SELECT Id, UserName, Email, Homepage, Created, Text, IP, Browser FROM Messages 
                ORDER BY {0} {1}
             	OFFSET {2} ROWS
	            FETCH NEXT {3} ROWS ONLY
            ";

            sqlString = string.Format(sqlString, 
                string.IsNullOrEmpty(sortPropertyName) || !_availableProperties.Contains(sortPropertyName)? "UserName" : sortPropertyName,
                sortOrder == SortOrder.Descending ? "DESC" : "ASC",
                (page-1)*pageSize,
                pageSize
                );

            var sqlCommand = new SqlCommand(sqlString, _connection);
            var result = new List<Message>();
            using (var reader = sqlCommand.ExecuteReader())
            {
                // И дальше бежим по полям dataReader пока не закончится эта канитель
                while (reader.Read())
                {
                    var message = new Message()
                                  {
                                      Id = int.Parse(reader["Id"].ToString()),
                                      UserName = reader["UserName"].ToString(),
                                      Email = reader["Email"].ToString(),
                                      HomePage = reader["HomePage"].ToString(),
                                      Created = DateTime.Parse(reader["Created"].ToString()),
                                      Text = reader["Text"].ToString(),
                                      IP = reader["IP"].ToString(),
                                      Browser = reader["Browser"].ToString()
                                  };   
                    result.Add(message);
                }
            }

            return result;
        }

        public int GetPageCount(int pageSize=DefaultPageSize)
        {
            var sqlString = "SELECT COUNT(*) FROM Messages";
            var sqlCommand = new SqlCommand(sqlString, _connection);

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (!reader.Read())
                {
                    return 0;
                }

                var allCount = int.Parse(reader[0].ToString());

                return allCount % pageSize == 0 
                    ? allCount / pageSize 
                    : allCount / pageSize + 1;
            }
        }

        public void Dispose()
        {
            if (_connection.State == ConnectionState.Open)
            {
                _connection.Close();
            }
            _connection.Dispose();
        }
    }
}